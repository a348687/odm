const jwt=require('jsonwebtoken');

class Auth {
    constructor(){
        if(typeof Auth.instance === 'object'){
            return Auth.instance;
        }
        Auth.instance = this; 
        return this;
    }
    JwtKey="13641c2867a5c5278d948187c0b75cb1";

    check(token){
        return jwt.verify(token, this.JwtKey);
    }

}
module.exports = Auth;
