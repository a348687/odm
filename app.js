var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose= require('mongoose');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var actorsRouter = require('./routes/actors');
const directorsRouter = require('./routes/directors');
const genresRouter = require('./routes/genres');
const membersRouter = require('./routes/members');
const moviesRouter = require('./routes/movies');
const copiesRouter = require('./routes/copies');
const bookingsRouter = require('./routes/bookings');
const awaitListsRouter = require('./routes/awaitLists');
const{ expressjwt }= require('express-jwt');
const config = require('config');
const rolesRouter = require('./routes/roles');

const JwtKey=config.get("secret.key");


var app = express();
//mongodb://<dbUser>?:<dbPass>?@<url>:<port>/<dbName>
const url = config.get("dbChain");
mongoose.connect(url);

const db = mongoose.connection;
db.on('open', ()=> {
  console.log("Connection OK");
});

db.on('error', ()=> {
  console.log("Connection Failed")
})

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(expressjwt({secret:JwtKey, algorithms:['HS256']}).unless({path:['/login', '/users']}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/directors', directorsRouter);
app.use('/actors', actorsRouter);
app.use('./genres', genresRouter);
app.use('./members', membersRouter);
app.use('./movies', moviesRouter);
app.use('./copies', copiesRouter);
app.use('./bookings', bookingsRouter);
app.use('./awaitLists', awaitListsRouter);
app.use('./roles', rolesRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
