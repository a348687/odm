const express = require('express');
const Copy = require('../models/copy');
const Member = require('../models/member');
const Booking = require('../models/booking')


async function create(req, res, next){
    const date = req.body.date;
    const memberId= req.body.memberId;
    const copyId= req.body.copyId;
    let member = await Member.findOne({"_id": memberId});
    let copy = await Copy.findOne({"_id": copyId});


    let booking = new Booking({
        date: date,
        member:member,
        copy: copy
        

    });

    booking.save().then(obj => res.status(200).json({
        msg: "Reserva  almacenada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la reserva",
        obj: ex
    }));

}

function list(req, res, next) {
  Booking.find().then(objs => res.stauts(200).json({
    msg:"Lista de reserva",
    obj:objs
  })).catch(ex => res.status(500).json({
    msg: "No se pudo consultar la lista de reserva",
    obj:ex
  }));
  }

  function index(req, res, next){
    const id=req.params.id;
    Booking.findOne({"_id":id}).then(obj => res.stauts(200).json({
      msg:`Reserva con el id ${id}`,
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo consultar la lista de reservas",
      obj:ex
    }));

  }


  function replace (req, res, next){
    const id= req.params.id;
    let date = req.body.date ? req.body.date : "";
    let memberId = req.body.memberId ? req.body.memberId : "";
    let copyId = req.body.copyId ? req.body.copyId : "";
    

    let booking = new Object({
      _date:date,
      _member:memberId,
      _copy:copyId
      
    });

    Booking.findOneAndUpdate({"_id":id}, booking,{new:true})
            .then(obj => res.status(200).json({
              msg: "Reserva remplazada correctamente",
              obj:obj
            })).catch(ex => res.status(500).json({
              msg: "No se pudo remplazar la reserva correctamente",
              obj:ex
            }));


  }

  function update(req, res, next){
    const id = req.params.id;
    let date = req.body.date;
    let memberId = req.body.memberId;
    let copyId=req.body.copyId;
    

    let booking = new Object();
    if (date)booking._date=date;
    if(memberId)booking._member=memberId;
    if(copyId)booking._copy=copyId;

    Booking.findOneAndUpdate({"_id":id}, booking).then(obj => res.status(200).json({
      msg:"Reserva actualizada correctamente",
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo remplazar la reserva correctamente",
      obj:ex
    }));

  }

  function destroy(req, res, next){
    const id =req.params.id;
    Booking.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
      msg:"Reserva eliminado correctamente",
      obj: obj

    })).catch(ex => res.status(500).json({
      msg: "No se pudo eliminar la reserva",
      obj:ex
    }));

    

  }

  
  module.exports={
    create,list, index, replace, update, destroy
  };