const express = require('express');
const Director = require('../models/director');
const Genre = require('../models/genre');
const Movie = require('../models/movie');

async function create(req, res, next){
    const title = req.body.title;
    const directorId= req.body.directorId;
    let director = await Director.findOne({"_id": directorId});
    let movie = new Movie({
        title: title,
        director:director

    });

    movie.save().then(obj => res.status(200).json({
        msg: "Pelicula almacenado correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "NO se pudo crear la pelicula",
        obj: ex
    }));

}

function list(req, res, next) {
    Movie.find().populate("_director").then(objs => res.stauts(200).json({
      msg:"Lista de peliculas",
      obj:objs
    })).catch(ex => res.status(500).json({
      msg: "No se pudo consultar la lista de peliculas",
      obj:ex
    }));
  }//populate hace que aparezca el director como tal y no su ID

  function index(req, res, next){
    const id=req.params.id;
    Director.findOne({"_id":id}).then(obj => res.stauts(200).json({
      msg:`Director con el id ${id}`,
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo consultar la lista de directores",
      obj:ex
    }));

  }


  function replace (req, res, next){
    const id= req.params.id;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";

    let director = new Object({
      _name:name,
      _lastName:lastName
    });

    Director.findOneAndUpdate({"_id":id}, director,{new:true})
            .then(obj => res.status(200).json({
              msg: "Director remplazado correctamente",
              obj:obj
            })).catch(ex => res.status(500).json({
              msg: "No se pudo remplazar el director correctamente",
              obj:ex
            }));


  }

  function update(req, res, next){
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

    let director = new Object();
    if (name) director._name=name;
    if(lastName)director._lastName=lastName;

    Director.findOneAndUpdate({"_id":id}, director).then(obj => res.status(200).json({
      msg:"Director actualizado correctamente",
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo remplazar el director correctamente",
      obj:ex
    }));

  }

  function destroy(req, res, next){
    const id =req.params.id;
    Director.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
      masg:"Director eliminado correctamente",
      obj: obj

    })).catch(ex => res.status(500).json({
      msg: "No se pudo eliminar el director",
      obj:ex
    }));

  }

  module.exports={
    create,list, index, replace, update, destroy
  };