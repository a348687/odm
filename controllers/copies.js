const express = require('express');
const Copy = require('../models/copy');
const Movie = require('../models/movie');


async function create(req, res, next){
    const number = req.body.number;
    const format= req.body.format;
    const status=req.body.status;
    const movieId= req.body.movieId;
    let movie = await Movie.findOne({"_id": movieId});

    let copy = new Copy({
        number: number,
        format:format,
        status: status,
        movie: movie

    });

    copy.save().then(obj => res.status(200).json({
        msg: "Copia almacenada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la copia",
        obj: ex
    }));

}

function list(req, res, next) {
  Copy.find().then(objs => res.stauts(200).json({
    msg:"Lista de usuarios",
    obj:objs
  })).catch(ex => res.status(500).json({
    msg: "No se pudo consultar la lista de usuarios",
    obj:ex
  }));
  }

  function index(req, res, next){
    const id=req.params.id;
    Copy.findOne({"_id":id}).then(obj => res.stauts(200).json({
      msg:`Copia con el id ${id}`,
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo consultar la lista de copias",
      obj:ex
    }));

  }


  function replace (req, res, next){
    const id= req.params.id;
    let number = req.body.number ? req.body.number : "";
    let format = req.body.format ? req.body.format : "";
    let status = req.body.status ? req.body.status : "";
    let movieId= req.body.movieId ? req.body.movieId : "";

    let copy = new Object({
      _number:number,
      _format:format,
      _status:status,
      _movie: movieId
    });

    Copy.findOneAndUpdate({"_id":id}, copy,{new:true})
            .then(obj => res.status(200).json({
              msg: "Copia remplazada correctamente",
              obj:obj
            })).catch(ex => res.status(500).json({
              msg: "No se pudo remplazar la copia correctamente",
              obj:ex
            }));


  }

  function update(req, res, next){
    const id = req.params.id;
    let number = req.body.number;
    let format = req.body.format;
    let status=req.body.status;
    let movieId= req.body.movieId;

    let copy = new Object();
    if (number)copy._name=number;
    if(format)copy._fomart=format;
    if(movieId)copy._movie=movieId;

    Copy.findOneAndUpdate({"_id":id}, copy).then(obj => res.status(200).json({
      msg:"Copia actualizada correctamente",
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo remplazar la copia correctamente",
      obj:ex
    }));

  }

  function destroy(req, res, next){
    const id =req.params.id;
    Copy.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
      msg:"Copia eliminado correctamente",
      obj: obj

    })).catch(ex => res.status(500).json({
      msg: "No se pudo eliminar la copia",
      obj:ex
    }));

    

  }

  
  module.exports={
    create,list, index, replace, update, destroy
  };