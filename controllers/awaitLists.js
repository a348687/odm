const express = require('express');
const Movie = require('../models/movie');
const Member = require('../models/member');
const AwaitList = require('../models/awaitList')


async function create(req, res, next){
    
    const memberId= req.body.memberId;
    const movieId= req.body.movieId;
    let member = await Member.findOne({"_id": memberId});
    let movie = await Movie.findOne({"_id": movieId});


    let awaitList = new AwaitList({
        date: date,
        member:member,
        movie: movie
        

    });

    awaitList.save().then(obj => res.status(200).json({
        msg: "Lista de espera  almacenada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la lista de espera",
        obj: ex
    }));

}

function list(req, res, next) {
  AwaitList.find().then(objs => res.stauts(200).json({
    msg:"Lista de espera",
    obj:objs
  })).catch(ex => res.status(500).json({
    msg: "No se pudo consultar la lista de espera",
    obj:ex
  }));
  }

  function index(req, res, next){
    const id=req.params.id;
    AwaitList.findOne({"_id":id}).then(obj => res.stauts(200).json({
      msg:`Lista de espera con el id ${id}`,
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo consultar la lista de espera",
      obj:ex
    }));

  }


  function replace (req, res, next){
    const id= req.params.id;
    let memberId = req.body.memberId ? req.body.memberId : "";
    let movieId = req.body.movieId ? req.body.movieId : "";
    

    let awaitList = new Object({
      
      _member:memberId,
      _movie:movieId
      
    });

    AwaitList.findOneAndUpdate({"_id":id}, awaitList,{new:true})
            .then(obj => res.status(200).json({
              msg: "Lista de espera remplazada correctamente",
              obj:obj
            })).catch(ex => res.status(500).json({
              msg: "No se pudo remplazar la lista de espera correctamente",
              obj:ex
            }));


  }

  function update(req, res, next){
    const id = req.params.id;
    let memberId = req.body.memberId;
    let movieId=req.body.copyId;
    

    let awaitList = new Object();
    
    if(memberId)awaitList._member=memberId;
    if(movieId)awaitList._movie=movieId;

    AwaitList.findOneAndUpdate({"_id":id}, awaitList).then(obj => res.status(200).json({
      msg:"La lista de espera fue  actualizada correctamente",
      obj:obj
    })).catch(ex => res.status(500).json({
      msg: "No se pudo remplazar la lista de espera correctamente",
      obj:ex
    }));

  }

  function destroy(req, res, next){
    const id =req.params.id;
    AwaitList.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
      msg:"Lista de espera eliminado correctamente",
      obj: obj

    })).catch(ex => res.status(500).json({
      msg: "No se pudo eliminar la lista de espera",
      obj:ex
    }));

    

  }

  
  module.exports={
    create,list, index, replace, update, destroy
  };