const express=require('express');
const User=require('../models/user');
const bcrypt=require('bcrypt');
const jwt=require('jsonwebtoken');
const config=require('config');


//aqui se me paso algo
function home(req, res, next){
    res.render('index', {title: 'Express'});
}
function login(req, res, next){
   const email=req.body.email;
    const password=req.body.password;
    const JwtKey=config.get("secret.key");
    User.findOne({"_email":email}).then(user => {
        if(user){
            bcrypt.hash(password, user.salt, (err, hash)=>{
                if(err){
                    res.status(403).json({
                        msg:"usuario y contraseña incorrecto",
                        obj:err
                    });  
                }
                if(hash===user.password){
                    res.status(200).json({
                        msg:"Login ok",
                        obj:jwt.sign({data:user.data, exp: Math.floor(Date.now()/1000)+180}, JwtKey)//definir el armado de un token
                    })    
                }
                else{
                    res.status(403).json({
                        msg:"usuario y contraseña incorrecto",
                        obj:null
                    });
                }
            });
        }else{
            res.status(403).json({
                msg:"usuario y contraseña incorrecto" ,
                obj:null
            });
        }
    }).catch(ex => res.status(403).json({
        msg:"usuario y contraseña incorrecto",
        obj:ex
    }));
}

module.exports={home,login}