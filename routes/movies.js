const express = require('express');
const router= express.Router();
const controller = require('../controllers/movies');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('movies', 'CREATE'), controller.create);

router.get('/', authMiddleware('movies', 'READ'), controller.list);

router.get('/:id', authMiddleware('movies', 'READ'), controller.index);

router.put('/:id', authMiddleware('movies', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('movies', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('movies', 'DELETE'), controller.destroy);

module.exports=router;

