const express = require('express');
const router= express.Router();
const controller = require('../controllers/directors');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('director', 'CREATE'), controller.create);

router.get('/', authMiddleware('directors', 'READ'), controller.list);

router.get('/:id', authMiddleware('directors', 'READ'), controller.index);

router.put('/:id', authMiddleware('directors', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('directors', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('directors', 'DELETE'), controller.destroy);

module.exports=router;