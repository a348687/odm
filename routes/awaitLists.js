const express = require('express');
const router= express.Router();
const controller = require('../controllers/awaitLists');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('awaitLists', 'CREATE'), controller.create);

router.get('/', authMiddleware('awaitLists', 'READ'), controller.list);

router.get('/:id', authMiddleware('awaitLists', 'READ'), controller.index);

router.put('/:id', authMiddleware('awaitLists', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('awaitLists', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('awaitLists', 'DELETE'), controller.destroy);

module.exports=router;