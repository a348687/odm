var express = require('express');
var router = express.Router();
const controller = require('../controllers/members');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('members', 'CREATE'), controller.create);

router.get('/', authMiddleware('members', 'READ'), controller.list);

router.get('/:id', authMiddleware('members', 'READ'), controller.index);

router.put('/:id', authMiddleware('members', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('members', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('members', 'DELETE'), controller.destroy);


module.exports = router;