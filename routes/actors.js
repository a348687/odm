const express = require('express');
const router= express.Router();
const controller = require('../controllers/actors');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('actors', 'CREATE'), controller.create);

router.get('/', authMiddleware('actors', 'READ'), controller.list);

router.get('/:id', authMiddleware('actors', 'READ'), controller.index);

router.put('/:id', authMiddleware('actors', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('actors', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('actors', 'DELETE'), controller.destroy);

module.exports=router;