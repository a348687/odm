const express = require('express');
const router= express.Router();
const controller = require('../controllers/copies');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('copies', 'CREATE'), controller.create);

router.get('/', authMiddleware('copies', 'READ'), controller.list);

router.get('/:id', authMiddleware('copies', 'READ'), controller.index);

router.put('/:id', authMiddleware('copies', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('copies', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('copies', 'DELETE'), controller.destroy);

module.exports=router;