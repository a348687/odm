const express = require('express');
const router= express.Router();
const controller = require('../controllers/bookings');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('bookings', 'CREATE'), controller.create);

router.get('/', authMiddleware('bookings', 'READ'), controller.list);

router.get('/:id', authMiddleware('bookings', 'READ'), controller.index);

router.put('/:id', authMiddleware('bookings', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('bookings', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('bookings', 'DELETE'), controller.destroy);

module.exports=router;