const express = require('express');
const router= express.Router();
const controller = require('../controllers/genres');
const authMiddleware = require('../authMiddleware');

router.post('/', authMiddleware('genres', 'CREATE'), controller.create);

router.get('/', authMiddleware('genres', 'READ'), controller.list);

router.get('/:id', authMiddleware('genres', 'READ'), controller.index);

router.put('/:id', authMiddleware('genres', 'UPDATE'), controller.replace);

router.patch('/:id', authMiddleware('genres', 'UPDATE'), controller.update);

router.delete('/:id', authMiddleware('genres', 'DELETE'), controller.destroy);

module.exports=router;