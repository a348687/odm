### Mi primera API con express + ODM.

Web Plataforms.

### Unidad II Programación web de parte del servidor.

### Base de datos a utilizar:

MongoDB

### Autor.

**Jocelyn Soto Avila**

**Matrícula: 348687**

### Docente.

Ing. Luis Antonio Ramírez Martínez

### Descripción del ejercicio.

`Vamos a construir todos los servicios web relacionados con el ejemplo del video club, basado en el esquema cree para cada modelo todo su CRUD. Una vez terminado suba su proyecto a gitlab en una rama diferente a master. Suba la liga del proyecto una vez terminado.`
